---
title: Tentang Kami
subtitle: >-
  Beberapa hal yang perlu Anda ketehui mengapa kami membuat spin-off dari Manjaro GNU/Linux.
img_path: images/cover-manjaro-x.webp
layout: page
menu:
  secondary:
    weight: 2
    name: Tentang Kami
---

## Mengapa Memilih Manjaro-X?

**Manjaro-X** merupakan pemasang Manjaro Linux yang lengkap untuk orang awam, desainer, pengajar, animator, dan pengembang aplikasi GTK & QT. Setelah memasang Manjaro-X versi rilis sistem operasi akan kembali menggunakan Manjaro linux. Sebab Manjaro-X bukanlah distribusi dengan branding setelah dipasang, melainkan sistem operasi Manjaro Linux yang di bundel kembali untuk kepraktisan penggunaan.

Tujuan Manjaro-X ini hanyalah untuk kepentingan pendidikan GNU/Linux semata, agar tercapainya kemudahan dan kenyamanan bagi pengguna yang pertama kali melihat sistem operasi GNU/Linux. Tujuan lainnya Manjaro-X ini untuk memperkaya jenis distribusi Manjaro Linux tek resmi sebagai sistem operasi GNU/Linux yang sederhana, kuat, dan elegan.

Di Manjaro-X beberapa tema dan gambar dinding khas Manjaro dihilangkan sehingga tampilan Manjaro-X lebih bersih dan natural. Dengan demikian akan mengembalikan _cita rasa_ Lingkungan Destop (DE) itu sendiri.

## Spin-Off

Dengan adanya Spin-Off diharapkan lebih fokus pada kebutuhan pengguna. Sehingga pengguna tidak lagi direpotkan dengan memilih aplikasi yang dibutuhkan. 5 Jenis Spin-Off Manjaro-X antara lain:

* Developer: Pengembang aplikasi.
* Design Suite: Desainer, ilustrator, animator, penyunting video.
* Education: Diperuntukkan pendidikan anak usia dini dan sekolah dasar.
* CAD: Arsitek, teknik sipil.
* Finance: Finansial, perbankan.

## Tim pengembang

### Hervy Qurrotul Ainur Rozi - Developer & Maintener Web

Sebagai Tim Pengembang utama. Perilis ISO versi terbaru, mengelola vps server, mengatur paket dan depedensi. Disisi lain sebagai Desainer di [Vectara.ID](https://vectara.id) dan Ilustrator yang menggunakan FLOSS (Free/Libre Open Source Software). Juga sebagai kontributor di openSUSE, openSUSE ID, LibreOffice ID, GNOME ID, dan Creative Common ID.

Kontak:

* Telegram : **[@hervyqa](https://t.me/hervyqa)**
* Twitter : **[@hervyqa](https://twitter.com/hervyqa)**
* Instagram : **[@hervyqa](https://instagram.com/hervyqa)**
* Mastodon : **[@hervyqa](https://mastodon.art/@hervyqa)**
* Gitlab : **[hervyqa](https://gitlab.com/hervyqa)**
* Gitlab GNOME : **[hervyqa](https://gitlab.gnome.org/hervyqa)**
* Github : **[hervyqa](https://github.com/hervyqa)**
* Surel : **[hervyqa@gmail.com](mailto:hervyqa@gmail.com)**
* Laman : **[https://hervyqa.com](https://hervyqa.com)**
