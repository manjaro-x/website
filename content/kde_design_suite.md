---
title: Unduh KDE Design Suite
sections:
  - section_id: single
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/kde_design_suite.webp
    title: Unduh KDE Design Suite
    content: >-
      Silahkan klik Unduh ISO untuk mulai mengunduh.
      
      ## Sesi Masuk
      
        |||
        |:--:|:--:|:--:|
        |**Nama Pengguna**|**Kata Sandi**|
        |user|user|
      
      ## Checksum
      
        ||||
        |:--:|:--:|:--:|:--:|
        |**MD**|**SHA1**|**SHA256**|
        | d2143439030 58afc7b78c7 b15c207f1b | 4c7a33226d1 4ab160f7dab 8232ee4d133 dd82aa4 | 834b5ee7b45e 347ff420422a ab6accf677 e681664bb b1bc14d424 e4ff57719d9 |

    actions:
      - label: Unduh ISO
        url: 'https://manjaro-x.id/release/20.1/manjaro-x-kde-design-suite-20.1-linux54-x86_64.iso'
      - label: Lihat Paket
        url: 'https://manjaro-x.id/release/20.1/manjaro-x-kde-design-suite-20.1-linux54-x86_64-pkgs.txt'
  - section_id: call-to-action
    component: cta_block.html
    type: ctablock
    title: Charity OS!
    subtitle: "Manjaro-X bisa didapatkan/diunduh secara gratis, namun membuatnya tidaklah gratis dan mudah. Kami sangat berterima kasih kepada Anda yang telah menggunakan Manjaro-X sebagai sistem operasi utama Anda. Jika dirasa Manjaro-X sangat bermanfaat untuk Anda secara pribadi maupun untuk Perusahaan /Lembaga /Universitas, Anda dapat mendonasikan kepada para pengembang untuk pembangunan dan pengujian Manjaro-X agar perkembangannya bisa berkelanjutan."
    actions:
      - label: Yuk, Donasi!
        url: /donation
---
