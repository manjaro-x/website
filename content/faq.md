---
title: Tanya Jawab
sections:
  - section_id: faq
    component: faq_block.html
    type: faqblock
    bg: gray
    title: Tanya Jawab Seputar Manjaro-X
    subtitle: 'Diracik untuk kepraktisan penggunaan dan diperuntukkan pengguna awam.'
    faqitems:
      - question: Apa Kelebihan Manjaro-X?
        answer: >-
          Menggunakan LTS Kernel, sudah ada multimedia, alat perkantoran, dan ada KDE Spin-Off untuk kebutuhan produktif.
      - question: Manjaro-X untuk siapa?
        answer: >-
          Untuk awam, siswa, guru, dosen, desainer, ilustrator, animator, arsitek, dan akuntan.
      - question: Apa Spesifikasi Manjaro-X GNOME?
        answer: >-
          * Memiliki diska kosong sekitar 25 GB
          
          * Memiliki RAM sekitar 4 GB
          
          * Memiliki resolusi layar minimal 1366:768px
      - question: Apa Spesifikasi Manjaro-X KDE Plasma?
        answer: >-
          * Memiliki diska kosong sekitar 25 GB
          
          * Memiliki RAM sekitar 2 GB
          
          * Memiliki resolusi layar minimal 1366:768px
      - question: Apakah saya bisa mengganti Windows dengan Manjaro-X?
        answer: >-
          Ya, tentu saja bisa.
      - question: Apakah saya bisa dualboot Windows dan Manjaro-X?
        answer: >-
          Ya, tentu saja bisa. tetapi tidak disarankan.
      - question: Apakah Manjaro-X medukung UEFI?
        answer: >-
          Ya, tentu saja mendukung.
      - question: Apakah saya dapat berkontribusi untuk Manjaro-X?
        answer: >-
          Ya, tentu saja Anda dapat. Silahkan mengunjungi profil [GitLab](https://gitlab.com/manjaro-x/manjaro-x-profil).
layout: pricing
menu:
  main:
    weight: 7
    name: Tanya Jawab
---
