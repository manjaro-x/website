---
title: Yuk, Donasi!
subtitle: >-
  
layout: page
---

![Cover Manjaro-X](cover-manjaro-x.webp)

Manjaro-X merupakan salah satu bentuk kepedulian terhadap sesama yang diwujudkan dengan charity OS, sehingga penggalangan donasi akan disalurkan ke [Lembaga ACT (Aksi Cepat Tanggap)](https://act.id/) korban bencana alam, dan sebagian lagi untuk kebutuhan maintenance developer.

Manjaro-X bisa didapatkan/diunduh secara gratis, namun membuatnya tidaklah gratis dan mudah. Kami sangat berterima kasih kepada Anda yang telah menggunakan Manjaro-X sebagai sistem operasi utama Anda. Jika dirasa Manjaro-X sangat bermanfaat untuk Anda secara pribadi maupun untuk perusahaan/lembaga/universitas, Anda dapat mendonasikan kepada para pengembang untuk pembangunan dan pengujian Manjaro-X agar perkembangannya bisa berkelanjutan.

Donasi Anda bisa melalui:

* **JENIUS** : **$hervyqa**
* **BTPN** : **90014450043** An. Hervy Qurrotul Ainur Rozi (kode bank: 213)
* **PayPal** : **[paypal.me/hervyqa](https://paypal.me/hervyqa)**

### Informasi

* **Surel: [os.manjaro.x@gmail.com](mailto:os.manjaro.x@gmail.com)**
* **Komunitas Telegram: [Manjaro-X](https://t.me/manjaro_x)**
* **Kanal Telegram: [Manjaro-X News](https://t.me/manjaro_x)**
