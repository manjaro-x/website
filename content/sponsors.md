---
title: Sponsors
layout: page
menu:
  main:
    weight: 8
    name: Sponsors
---

Kami selaku tim Manjaro-X mengucapkan Terima kasih atas bantuan sumber daya. Terutama Pengguna dan Para Donatur yang selalu mendukung pengembangan Manjaro-X.

***

## PT. Sakti Kinerja Kolaborasindo

[![logo-ptskk](/images/sponsors/ptskk.webp)](https://ptskk.id)

***

### Berkomitmen Mengembangkan Teknologi Koperasi

PT SKK membantu koperasi-koperasi untuk tumbuh berkembang dengan pemanfaatan teknologi informasi yang tepat guna dan terpercaya, sehingga koperasi-koperasi bisa berkerja dengan tingkat efisiensi yang tinggi. Selain itu, koperasi-koperasi bisa saling terhubung satu sama lain demi meningkatkan produktifitasnya.

### Pengembangan Aplikasi yang Berorientasi pada Pengguna

Demi memberikan pelayanan yang maksimal, kami terus berupaya untuk mengembangkan aplikasi-aplikasi perkoperasian agar lebih ramah pengguna dengan berbagai latar belakang yang beragam di seluruh Indonesia.

Aplikasi yang dikembangkan akan disesuaikan dengan kebutuhan pengguna koperasi sehingga teknologi yang dikembangkan menjadi tepat guna.

### Sistem Core Banking Koperasi Berbasis Teknologi Sumber Terbuka

Demi memberikan pelayanan yang maksimal, kami terus berupaya untuk mengembangkan sistem core banking koperasi agar lebih ramah pengguna dengan berbagai latar belakang yang beragam di seluruh Indonesia.

Sistem yang dikembangkan ini berbasis teknologi sumber terbuka, sehingga bisa diaudit keamanannya.
