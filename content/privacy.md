---
title: Privasi pengguna
subtitle: >-
  Kebijakan Privasi ini mengatur cara Manjaro-X.
img_path: images/cover-manjaro-x.webp
layout: page
menu:
  secondary:
    weight: 3
    name: Privasi pengguna
---

## Siapa kita

Kebijakan Privasi ini mengatur cara Manjaro-X mengumpulkan, menggunakan, mengelola dan mengungkapkan informasi yang dikumpulkan dari pengguna (masing-masing, "Pengguna") dari situs web.

Data pribadi apa yang kami kumpulkan dan alasan mengapa kami kumpulkan:

## Informasi identifikasi pribadi

Kami dapat mengumpulkan informasi identifikasi pribadi dari Pengguna dengan berbagai cara, termasuk, tetapi tidak terbatas pada, ketika Pengguna mengunjungi situs kami, mendaftar di situs, memesan, mengisi formulir, menanggapi survei, dan sehubungan dengan kegiatan, layanan, fitur, atau sumber daya lain yang kami sediakan di Situs kami. Pengguna dapat mengunjungi Situs kami secara anonim. Kami akan mengumpulkan informasi identifikasi pribadi dari Pengguna hanya jika mereka secara sukarela menyerahkan informasi tersebut kepada kami. Pengguna dapat selalu menolak untuk memberikan informasi identifikasi pribadi, kecuali bahwa hal itu dapat mencegah mereka untuk terlibat dalam kegiatan terkait Situs tertentu.

## Informasi identifikasi non-pribadi

Kami dapat mengumpulkan informasi identifikasi non-pribadi tentang Pengguna setiap kali mereka berinteraksi dengan Situs kami. Informasi identifikasi non-pribadi dapat mencakup nama browser, jenis komputer dan informasi teknis tentang cara koneksi Pengguna ke Situs kami, seperti sistem operasi dan penyedia layanan Internet yang digunakan dan informasi serupa lainnya.

## Kuki

Situs kami dapat menggunakan "cookies" untuk meningkatkan pengalaman Pengguna. Browser web pengguna menempatkan cookie pada hard drive mereka untuk tujuan pencatatan dan terkadang untuk melacak informasi tentang mereka. Pengguna dapat memilih untuk mengatur browser web mereka untuk menolak cookie atau untuk mengingatkan Anda ketika cookie dikirim. Jika mereka melakukannya, perhatikan bahwa beberapa bagian Situs mungkin tidak berfungsi dengan benar.

## Konten yang disematkan dari situs web lain

Artikel di situs ini dapat mencakup konten yang disematkan (mis. Video, gambar, artikel, dll.). Konten yang disematkan dari situs web lain berperilaku sama persis seperti jika pengunjung telah mengunjungi situs web lain.

Situs web ini dapat mengumpulkan data tentang Anda, menggunakan cookie, menanamkan pelacakan pihak ketiga tambahan, dan memantau interaksi Anda dengan konten yang disematkan, termasuk melacak interaksi Anda dengan konten yang disematkan jika Anda memiliki akun dan login ke situs web tersebut.

## Media

Jika Anda mengunggah gambar ke situs web, Anda harus menghindari mengunggah gambar dengan data lokasi tertanam (EXIF GPS). Pengunjung situs web dapat mengunduh dan mengekstrak data lokasi apa pun dari gambar di situs web.

## Layanan Pembayaran

Kami menawarkan untuk menyumbang ke proyek kami melalui [PayPal](https://paypal.com). Penyedia layanan ini adalah PayPal (Eropa) S.à.r.l. et Cie, S.C.A., 22-24 Boulevard Royal, L-2449 Luksemburg. Ketika Anda memilih untuk menyumbang melalui PayPal, data pembayaran yang Anda masukkan akan ditransfer ke Paypal. Transfer data akan diproses sesuai dengan Peraturan Perlindungan Data Umum Eropa. Pemrosesan data dapat dibatalkan kapan saja. Namun pembatalan ini tidak mencerminkan transfer data yang sudah diproses dari masa lalu. Pelajari lebih lanjut tentang Kebijakan Privasi PayPal dengan mengunjungi halaman web mereka.

## Bagaimana kami menggunakan informasi yang dikumpulkan

Situs web kami dapat mengumpulkan dan menggunakan informasi pribadi Pengguna untuk tujuan berikut:

- Untuk menjalankan dan mengoperasikan Situs kami, Kami mungkin memerlukan informasi Anda untuk menampilkan konten di Situs dengan benar.
- Untuk meningkatkan layanan pelanggan Informasi yang Anda berikan membantu kami menanggapi permintaan layanan pelanggan Anda dan kebutuhan dukungan lebih efisien.
- Untuk mempersonalisasikan pengalaman pengguna Kami dapat menggunakan informasi secara agregat untuk memahami bagaimana Pengguna kami sebagai grup menggunakan layanan dan sumber daya yang disediakan di Situs kami.
- Untuk meningkatkan Situs kami, Kami dapat menggunakan umpan balik yang Anda berikan untuk meningkatkan produk dan layanan kami.
- Untuk menjalankan promosi, kontes, survei, atau fitur Situs lainnya Untuk mengirim informasi Pengguna yang mereka setujui untuk menerima topik yang menurut kami menarik bagi mereka.
- Untuk mengirim email berkala Kami dapat menggunakan alamat email untuk mengirim informasi Pengguna dan pembaruan terkait pesanan mereka. Ini juga dapat digunakan untuk menjawab pertanyaan, pertanyaan, dan / atau permintaan lainnya.

## Berbagi informasi pribadi Anda

Kami tidak menjual, memperdagangkan, atau menyewakan informasi identifikasi pribadi Pengguna kepada orang lain. Kami dapat membagikan informasi demografis agregat generik yang tidak terkait dengan informasi identifikasi pribadi apa pun tentang pengunjung dan pengguna dengan mitra bisnis kami dan afiliasi tepercaya untuk tujuan yang diuraikan di atas. Kami dapat menggunakan penyedia layanan pihak ketiga untuk membantu kami mengoperasikan bisnis kami dan Situs atau mengelola kegiatan atas nama kami, seperti mengirimkan buletin atau survei. Kami dapat membagikan informasi Anda dengan pihak ketiga ini untuk tujuan terbatas asalkan Anda telah memberikan izin kepada kami.

## Berapa lama kami menyimpan data Anda

Jika Anda meninggalkan komentar, komentar dan metadata-nya tetap dipertahankan tanpa batas. Ini agar kami dapat mengenali dan menyetujui komentar tindak lanjut secara otomatis alih-alih menahannya dalam antrean moderasi.

## Perubahan pada kebijakan privasi ini

Situs web kami memiliki keleluasaan untuk memperbarui kebijakan privasi ini kapan saja. Ketika kami melakukannya, kami akan memposting pemberitahuan di halaman utama Situs kami, merevisi tanggal yang diperbarui di bagian bawah halaman ini. Kami mendorong Pengguna untuk sering memeriksa halaman ini jika ada perubahan agar tetap mendapat informasi tentang bagaimana kami membantu melindungi informasi pribadi yang kami kumpulkan. Anda mengakui dan setuju bahwa Anda bertanggung jawab untuk meninjau kebijakan privasi ini secara berkala dan mengetahui adanya modifikasi.

## Penerimaan Anda atas persyaratan ini

Dengan menggunakan Situs ini, Anda menandakan penerimaan Anda terhadap kebijakan ini. Jika Anda tidak menyetujui kebijakan ini, jangan gunakan Situs kami. Jika Anda terus menggunakan Situs setelah posting perubahan pada kebijakan ini akan dianggap Anda menerima perubahan tersebut.

## Menghubungi kami

Jika Anda memiliki pertanyaan tentang Kebijakan Privasi ini, praktik situs ini, atau transaksi Anda dengan situs ini, silakan hubungi kami melalui surel [os.manjaro.x@gmail.com](mailto:os.manjaro.x@gmail.com).
