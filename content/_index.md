---
title: Beranda
sections:
  - section_id: hero
    component: hero_block.html
    type: heroblock
    image: images/hero.webp
    title: Sederhana, Variatif, dan Elegan.
    content: >-
      Manjaro-X adalah Distribusi Spin-off dari Manjaro GNU/Linux untuk kebutuhan Pengembang, Desain Grafis, Musisi, Arsitek, dan Akuntan. Menggunakan kernel LTS dan Rolling Rilis.
    actions:
      - label: Unduh v20.1
        url: /download
  - section_id: features
    component: features_block.html
    type: featuresblock
    bg: gray
    title: Apa Kelebihannya?
    subtitle: >-
      Diracik untuk kepraktisan penggunaan dan diperuntukkan pengguna awam.
    featureslist:
      - title: GNOME
        image: images/gnome.webp
        content: >-
          Kemudahan dan kesederhanaan sudah menjadi ciri khas GNOME. Tampilan yang kuat dan elegan. Disarankan untuk pengguna awam yang baru mengenal GNU/Linux.
        actions:
          - label: GNOME
            url: /gnome
      - title: KDE Plasma
        image: images/kde.webp
        content: >-
          KDE Plasma memiliki tampilan yang kaya fitur dan intuitif. Memiliki gaya klasik yang mudah dipahami oleh pengguna awam. Serta memiliki berbagai aplikasi penunjang kreatifitas.
        actions:
          - label: KDE Plasma
            url: /kde
      - title: KDE Spin-Off
        image: images/kde_spin_off.webp
        content: >-
          KDE Plasma Spin-Off diperuntukkan bagi yang produktif. Seperti Developer, Desainer, Ilustrator, Animator, Arsitek, dan Akuntan. Apakah Anda salah satu diantaranya? Cobalah varian Spin-off ini.
        actions:
          - label: KDE Spin-Off
            url: /spin_off
      - title: Kernel LTS
        image: images/kernel_lts.webp
        content: >-
          Manjaro-X lebih stabil karena menggunakan kernel LTS (long term support). Sehingga dukungan kernel lebih stabil dan performanya yang cepat.
      - title: Encoding Suara dan Film
        image: images/encoding.webp
        content: >-
          Manjaro-X sudah dibundle dengan dependensi suara dan film. Jadi, memutar berkas multimedia cukup dengan sekali klik.
      - title: Perangkat Lunak Perkantoran
        image: images/office.webp
        content: >-
          Manjaro-X sudah memaketkan perangkat lunak perkantoran LibreOffice yang lengkap. Mulai dari writer, calc, impress, draw dan database.
      - title: Keamanan Lebih Terjamin
        image: images/privasi.webp
        content: >-
          Manjaro-X lebih aman menjaga privasi pengguna karena didalamnya sudah terinstall TOR dan terdapat beberapa extensi tambahan untuk browser. Sehingga keamanan berselancar di internet lebih terjamin.
  - section_id: reviews
    component: reviews_block.html
    type: reviewsblock
    bg: white
    title: Apa Kata Mereka?
    subtitle: >-
      Manjaro-X telah digunakan oleh kalangan pendidikan, FLOSS desainer, dan pengembang aplikasi.
    reviews:
      - author: Ibnu Bahtiar | @muslim.type
        avatar: images/review1.webp
        content: >-
          Kilat, stabil & bebas berekpresi! Manjaro-X adalah pengalaman baru bagi saya. Sebagai Designer, Manjaro-x Designer Suite bisa langsung digunakan dengan semua aplikasi yang sudah terpasang dan saya bisa mengkustomisasi lebih bebas daripada distro yang saya sudah coba sebelumnya!.
      - author: Abu Sahal Wisnu | @waditos
        avatar: images/review2.webp
        content: >-
          Stabil dan selalu bisa memakai software versi terbaru, bahkan karena stabilnya saya pede menggunakannya di Dunia Pendidikan.
      - author: Rizqi Nur Assyaufi | @bandithijo
        avatar: images/review3.webp
        content: >-
          Saya suka tujuannya Manjaro-X ini. Beberapa teman yang kurang bisa menghandle kerumitan Arch, memilih menggunakan Manjaro karena keluwesannya seperti Arch. Kehadiran Manjaro-X dapat memperkaya pilihan bagi pengguna awam.
  - section_id: call-to-action
    component: cta_block.html
    type: ctablock
    title: Charity OS!
    subtitle: Manjaro-X merupakan salah satu bentuk kepedulian terhadap sesama yang diwujudkan dengan charity OS, sehingga penggalangan donasi akan disalurkan ke Lembaga ACT (Aksi Cepat Tanggap) korban bencana alam, dan sebagian lagi untuk kebutuhan maintenance developer.
    actions:
      - label: Yuk, Donasi!
        url: /donation
  - section_id: recent-posts
    component: posts_block.html
    type: postsblock
    bg: gray
    title: Artikel Baru
layout: home
menu:
  main:
    weight: 1
    name: Beranda
---
