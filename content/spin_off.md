---
title: Spin-Off
sections:
  - section_id: spin_off
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/kde_developer.webp
    title: KDE Developer
    content: >-
     KDE Developer ditujukan kepada FrontEnd dan BackEnd Developer. Visual Code, Kdevelop, QTCreator, Neovim, Arduino, Lampp, NodeJS, NPM, dan Composer sudah tersedia didalamnya.
    actions:
      - label: Spin-Off Developer
        url: /kde_developer
  - section_id: features
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/kde_design_suite.webp
    title: KDE Design Suite
    content: >-
      KDE Design Suite ditujukan kepada bidang keahlian Desainer, Ilustrator, Animator, Video editor, dan Font Kreator. Design Suite sudah menyediakan Inkscape, Gimp, Krita, Kdenlive, OBS-studio, Fontforge, dan Scribus sebagai perangkat lunak andalan.
    actions:
      - label: Spin-Off Design Suite
        url: /kde_design_suite
  - section_id: features
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/kde_cad.webp
    title: KDE CAD
    content: >-
      KDE CAD untuk Arsitek dengan kebutuhan aplikasi CAD (Computer Aided Design). Didalam Spin-off ini sudah tersedia perangkat lunak LibreCAD, QCAD, dan OpensCAD.
    actions:
      - label: Spin-Off CAD
        url: /kde_cad
  - section_id: features
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/kde_finance.webp
    title: KDE Finance
    content: >-
      KDE Finance untuk bidang yang membutuhkan perangkat lunak finansial dan perbankan. Di dalamnya sudah tersedia aplikasi KMymoney personal finansial manajer, Skrooge, MoneyManagerEx, GNUCash, PSPP dan Homebank.
    actions:
      - label: Spin-Off Finance
        url: /kde_finance
  - section_id: features
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/kde_education.webp
    title: KDE Education
    content: >-
      KDE Education untuk anak-anak usia 2-10 tahun. Perangkat lunak Gcompris untuk belajar membaca, berhitung, dan mengetik. Kstars untuk planetaium, Blinker untuk motorik, Kgeography, dan masih banyak lagi.
    actions:
      - label: Spin-Off Education
        url: /kde_education
  - section_id: call-to-action
    component: cta_block.html
    type: ctablock
    title: Charity OS!
    subtitle: "Manjaro-X bisa didapatkan/diunduh secara gratis, namun membuatnya tidaklah gratis dan mudah. Kami sangat berterima kasih kepada Anda yang telah menggunakan Manjaro-X sebagai sistem operasi utama Anda. Jika dirasa Manjaro-X sangat bermanfaat untuk Anda secara pribadi maupun untuk Perusahaan /Lembaga /Universitas, Anda dapat mendonasikan kepada para pengembang untuk pembangunan dan pengujian Manjaro-X agar perkembangannya bisa berkelanjutan."
    actions:
      - label: Yuk, Donasi!
        url: /donation
layout: spin_off
menu:
  main:
    weight: 4
    name: Spin-Off
---
