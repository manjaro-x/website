---
title: Unduh KDE Education
sections:
  - section_id: single
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/kde_education.webp
    title: Unduh KDE Education
    content: >-
      Silahkan klik Unduh ISO untuk mulai mengunduh.
      
      ## Sesi Masuk
      
        |||
        |:--:|:--:|:--:|
        |**Nama Pengguna**|**Kata Sandi**|
        |user|user|
      
      ## Checksum
      
        ||||
        |:--:|:--:|:--:|:--:|
        |**MD**|**SHA1**|**SHA256**|
        | e39ceebb71e 8c07deb52bf fd7fe571c7 | 9b83d259e09 aa9b83e5aa2 286e72f5ab 9c8b45f2 | e9b46bf2526 22a9e843aed 74a48cbda6c 06ecd6e0891 a1561a6780 98d99d3458 |

    actions:
      - label: Unduh ISO
        url: 'https://manjaro-x.id/release/20.1/manjaro-x-kde-education-20.1-linux54-x86_64.iso'
      - label: Lihat Paket
        url: 'https://manjaro-x.id/release/20.1/manjaro-x-kde-education-20.1-linux54-x86_64-pkgs.txt'
  - section_id: call-to-action
    component: cta_block.html
    type: ctablock
    title: Charity OS!
    subtitle: "Manjaro-X bisa didapatkan/diunduh secara gratis, namun membuatnya tidaklah gratis dan mudah. Kami sangat berterima kasih kepada Anda yang telah menggunakan Manjaro-X sebagai sistem operasi utama Anda. Jika dirasa Manjaro-X sangat bermanfaat untuk Anda secara pribadi maupun untuk Perusahaan /Lembaga /Universitas, Anda dapat mendonasikan kepada para pengembang untuk pembangunan dan pengujian Manjaro-X agar perkembangannya bisa berkelanjutan."
    actions:
      - label: Yuk, Donasi!
        url: /donation
---
