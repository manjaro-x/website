---
title: Kontak
layout: contact
menu:
  secondary:
    weight: 4
    name: Kontak
---

Manjaro-X merupakan GNU/Linux yang jauh dari kesempurnaan. Kami berterima kasih apabila Anda berkenan memberikan komentar, kritikan dan saran!
