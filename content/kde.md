---
title: Unduh KDE Plasma
sections:
  - section_id: single
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/kde.webp
    title: Unduh KDE Plasma
    content: >-
      Silahkan klik Unduh ISO untuk mulai mengunduh.
      
      ## Sesi Masuk
      
        |||
        |:--:|:--:|:--:|
        |**Nama Pengguna**|**Kata Sandi**|
        |user|user|
      
      ## Checksum
      
        ||||
        |:--:|:--:|:--:|:--:|
        |**MD**|**SHA1**|**SHA256**|
        | 52281282d2e 64755d56d4 9da15e58aaf | cd46648587e 39bf6ac703 6a2f339e118 f9724ef4 | 8f44c262031 fe1ca3058bf 1e8da12cccb 98dcdc07c2 144039258ed 2f2af60e451 |

    actions:
      - label: Unduh ISO
        url: 'https://manjaro-x.id/release/20.1/manjaro-x-kde-20.1-linux54-x86_64.iso'
      - label: Lihat Paket
        url: 'https://manjaro-x.id/release/20.1/manjaro-x-kde-20.1-linux54-x86_64-pkgs.txt'
  - section_id: call-to-action
    component: cta_block.html
    type: ctablock
    title: Charity OS!
    subtitle: "Manjaro-X bisa didapatkan/diunduh secara gratis, namun membuatnya tidaklah gratis dan mudah. Kami sangat berterima kasih kepada Anda yang telah menggunakan Manjaro-X sebagai sistem operasi utama Anda. Jika dirasa Manjaro-X sangat bermanfaat untuk Anda secara pribadi maupun untuk Perusahaan /Lembaga /Universitas, Anda dapat mendonasikan kepada para pengembang untuk pembangunan dan pengujian Manjaro-X agar perkembangannya bisa berkelanjutan."
    actions:
      - label: Yuk, Donasi!
        url: /donation
---
