---
title: Unduh KDE Developer
sections:
  - section_id: single
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/kde_developer.webp
    title: Unduh KDE Developer
    content: >-
      Silahkan klik Unduh ISO untuk mulai mengunduh.
      
      ## Sesi Masuk
      
        |||
        |:--:|:--:|:--:|
        |**Nama Pengguna**|**Kata Sandi**|
        |user|user|
      
      ## Checksum
      
        ||||
        |:--:|:--:|:--:|:--:|
        |**MD**|**SHA1**|**SHA256**|
        | c87d9fc9038 eec044621c6 3c37e6f490f | 9fa88a10660 170f0644e5 a2c3474d46 8513b8c8a | 3a7b85ba80a 144d0e2c563 e3c753856fd 622134a6609b dcab824cdd9 007e3ea5 |

    actions:
      - label: Unduh ISO
        url: 'https://manjaro-x.id/release/20.1/manjaro-x-kde-developer-20.1-linux54-x86_64.iso'
      - label: Lihat Paket
        url: 'https://manjaro-x.id/release/20.1/manjaro-x-kde-developer-20.1-linux54-x86_64-pkgs.txt'
  - section_id: call-to-action
    component: cta_block.html
    type: ctablock
    title: Charity OS!
    subtitle: "Manjaro-X bisa didapatkan/diunduh secara gratis, namun membuatnya tidaklah gratis dan mudah. Kami sangat berterima kasih kepada Anda yang telah menggunakan Manjaro-X sebagai sistem operasi utama Anda. Jika dirasa Manjaro-X sangat bermanfaat untuk Anda secara pribadi maupun untuk Perusahaan /Lembaga /Universitas, Anda dapat mendonasikan kepada para pengembang untuk pembangunan dan pengujian Manjaro-X agar perkembangannya bisa berkelanjutan."
    actions:
      - label: Yuk, Donasi!
        url: /donation
---
