---
title: Cara membuat bootable Manjaro-X di GNU/Linux
subtitle: >-
  Membuat bootable di GNU/Linux banyak sekali perangkat lunak GUI yang bisa digunakan, seperti multiwriter, gnome-multi-writer, imagewriter, gnome-disks-utility.
layout: page
---

## 1. Bootable GNOME Disks

### Install GNOME Disks

|Nama Distro| Perintah|
| :-- | :-- |
|Ubuntu| `sudo apt install gnome-disks-utility`|
|openSUSE| `sudo zypper install gnome-disks-utility`|
|Fedora| `sudo dnf install gnome-disks-utility`|
|Centos| `su -c 'yum install gnome-disks-utility'`|
|Arch| `sudo pacman -S gnome-disks-utility`|

### Buka GNOME Disks

Buka GNOME Disks Utility kemudian klik bagian USB Drive.

![gnome-disks-utility](gnome-disks-utility-1.webp)

### Format USB Drive

Klik menu, kemudian pilih klik **Format Diska..**.

![gnome-disks-utility](gnome-disks-utility-2.webp)

Seteleh itu muncul dialog format. Pilih **Format**.

![gnome-disks-utility](gnome-disks-utility-3.webp)


Kemudian Muncul dialog peringatan Format Diska. Pilih **Format**.

![gnome-disks-utility](gnome-disks-utility-4.webp)

### Menyalin ISO Manjaro-X ke USB Drive

![gnome-disks-utility](gnome-disks-utility-5.webp)


Pada bagian Citra untuk dipulihkan, tekan kotak **Nihil**.

![gnome-disks-utility](gnome-disks-utility-6.webp)

Masukkan berkas ISO **Manjaro-X** kemudian pilih **Buka**.

![gnome-disks-utility](gnome-disks-utility-7.webp)

Setelah itu muncul dialog **Pulihkan Berkas Diska**, Klik **Mulai Pengembalian**

![gnome-disks-utility](gnome-disks-utility-8.webp)

Kemudian muncul dialog **Tulis berkas diska pada perangkat?**, pilih **Kembalikan**

![gnome-disks-utility](gnome-disks-utility-9.webp)

Tunggu proses hingga 100%. Proses menyalin akan memakan waktu sekitar 10 menit. Bersabarlah.
 
![gnome-disks-utility](gnome-disks-utility-10.webp)

### Selesai

Sekarang bootable dengan GNOME disks sudah bisa digunakan.

## 2. Bootable GNOME Multiwriter

### Install GNOME Multiwriter

|Nama Distro| Perintah|
| :-- | :-- |
|Ubuntu| `sudo apt install gnome-multi-writer`|
|openSUSE| `sudo zypper install gnome-multi-writer`|
|Fedora| `sudo dnf install gnome-multi-writer`|
|Centos| `su -c 'yum install gnome-multi-writer'`|
|Arch| `sudo pacman -S gnome-multi-writer`|

### Buka GNOME Multiwriter

Bukalah Aplikasi **MultiWriter**.

![multi-writer](multi-writer-1.webp)

### Pilih Berkas ISO

Pilih Berkas ISO Manjaro-X, kemudian pilih **Impor**.

![multi-writer](multi-writer-2.webp)

### Mulai menyalin

Kemudian pilih **Mulai Menyalin**.

![multi-writer](multi-writer-3.webp)

### Peringatan

Data-data Anda akan terhapus. Klik **Saya Paham**.

![multi-writer](multi-writer-4.webp)

### Proses menyalin

Tunggu proses hingga 100%. Proses ini akan memakan waktu sekitar 10 menit. Bersabarlah.

![multi-writer](multi-writer-5.webp)

### Selesai Menyalin

Menyalin ISO Manjaro-X ke USB Drive sudah selesai. Kini siap untuk dijadikan sebagai bootable.

![multi-writer](multi-writer-6.webp)

