---
title: Cara memformat bootable USB Drive
subtitle: >-
  Mengembalikan partisi flasdisk seperti normal kembali
layout: page
---

## Buka GNOME Disks

Buka GNOME Disks Utility kemudian klik bagian USB Drive.

![gnome-disks-utility](gnome-disks-utility-1.webp)

## Format USB Drive

Klik menu, kemudian pilih klik **Format Diska..**.

![gnome-disks-utility](gnome-disks-utility-2.webp)

Seteleh itu muncul dialog format. Pilih **Format**.

![gnome-disks-utility](gnome-disks-utility-3.webp)

Kemudian Muncul dialog peringatan Format Diska. Pilih **Format**.

![gnome-disks-utility](gnome-disks-utility-4.webp)

## Membuat Partisi FAT32 baru

Klik tanda Plus **+** untuk membuat partisi baru.

![gnome-disks-utility](gnome-disks-utility-5.webp)

Pilih Selanjutnya.

![gnome-disks-utility](gnome-disks-utility-6.webp)

Berilah Nama Volume USB Drive. Pilih **FAT32**. Kemudian klik **Buat**.

![gnome-disks-utility](gnome-disks-utility-7.webp)

Flasdisk sudah kembali Normal dengan partisi **FAT32**.

![gnome-disks-utility](gnome-disks-utility-8.webp)

Selesai.
