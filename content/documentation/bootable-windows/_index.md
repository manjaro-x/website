---
title: Cara membuat bootable Manjaro-X di Windows
subtitle: >-
  Membuat bootable di Windows dengan Rufus
layout: page
---

## Unduh Rufus

Kunjungi laman Rufus _**[https://rufus.ie/](https://rufus.ie/)**_ kemudian bagian Unduh, klik **Rufus versi terbaru**.

![Rufus](rufus-download.webp)

## Buka aplikasi Rufus

Buka aplikasi Rufus. Kemudian muncul tampilan seperti diatas.

![Rufus](rufus-1.webp)

## Pilih tujuan USB Drive

Pada bagian **Device** Pilihlah USB Drive Anda.

![Rufus](rufus-2.webp)


## Pilih ISO Manjaro-X

Pada bagian **Boot selection**. Klik **SELECT** kemudian masukkan berkas **.iso** dan klik **OK**.

![Rufus](rufus-3.webp)

## Pilih START

Setelah Anda yakin lokasi USB drive dan iso Manjaro-X sudah benar. Maka selanjutnya klik **START**. Jika muncul peringatan klik **OK**.

![Rufus](rufus-4.webp)

## Tunggu hingga selesai 100%

Tunggu proses hingga 100%. Proses ini akan memakan waktu sekitar 5-10 menit.

![Rufus](rufus-5.webp)

## Selesai.

Sekarang bootable dengan Rufus sudah bisa digunakan.

_Catatan:_

* Selain menggunakan **Rufus**, Anda dapat meggunakan **[Etcer](https://www.balena.io/etcher/)** sebagai aplikasi bootable di windows.
