---
title: Dokumentasi
subtitle: >-
  Sebelum melakukan instalasi sebaiknya persiapkan USB flasdisk untuk membuat bootable. Bagaimana caranya? yuk simak panduan singkat pemasangan Manjaro-X.
date: '2020-02-21'
layout: page
menu:
  main:
    weight: 6
    name: Dokumentasi
---

## 1. Spesifikasi

* Bootable DVD atau Flasdisk minimal 4 GB.
* Memiliki 2 GHz dual-core prosesor atau lebih.
* Memiliki RAM minimal 4 GB (Disarankan 8 GB).
* Memiliki Penyimpanan data lebih dari 25 GB.
* Memiliki Resolusi layar minimal 1366 x 768 px.

## 2. Persiapan Penting

**Pengguna Windows:**

+ Manjaro-X tidak dianjurkan untuk Dual-OS dengan **Sistem Operasi Windows**.
+ Cadangkan data penting Anda ke hardisk eksternal, apabila data anda masih menggunakan **NTFS**.
+ Cadangkan data Anda mulai dari direktori **C:/Users/$NAMAUSER**.

**Pengguna GNU/Linux:**

+ Jika Anda bermigrasi ke Manjaro-X pastikan Anda mempunyai partisi **/home**, Jika tidak punya maka cadangkan data USER Anda ke hardisk eksternal.

## 3. Cara membuat Bootable

Ketika membuat Bootable, cadangkan semua data penting Anda di dalam USB Flasdisk. Sebab perangkat lunak bootable akan menghapus seluruh data. Lakukanlah pencadangan ini jika ada data yang penting.

Kemudian buatlah bootable sesuai sistem operasi Anda:

* **[Cara membuat Bootable di GNU/Linux](/documentation/bootable-gnulinux)**

* **[Cara membuat Bootable di Windows](/documentation/bootable-windows)**

### Login

|||
|:--|:--|
|Nama pengguna|**user**|
|Kata sandi|**user**|

## 4. Mengatur UEFI/LEGACY

Aturlah boot manager anda dengan **USB Drives** sebagai boot order yang pertama. Secara umum menu Bios menggunakan tombol **Esc atau F2** sedangkan untuk boot order menggunakan **F9, F10, atau F12**.

Kemudian carilah informasi BIOS Anda telah mendukung **UEFI** atau tidak, jika tidak maka BIOS-nya masih menggunakan **LEGACY**.

![4.1-grub-manjaro-x](4.1-grub-manjaro-x.webp)

## 5. Manjaro-X Installer

Ketika sudah memasuki Live USB, pastikan semua hardware Anda bekerja, seperti Wifi, Bluetooth (Laptop), Touchpad, Kecerahan layar (Laptop), dan Suara.

Pilih Menu **Install Manjaro-X** dengan menekan tombol Super. Tunggulah beberapa saat hingga muncul **Manjaro-X Installer**.

![5.1-pasang](5.1-pasang.webp)

Jika Anda menggunakan Laptop pastikan adaptor pengisi daya Anda telah tertancap dan Koneksi Internet Anda sedang mati.

![5.2-pasang](5.2-pasang.webp)

## 6. Mengganti Bahasa

Pada tab **Welcome**, Gantilah pengaturan bahasa **American English** Menjadi **Indonesia**. kemudian klik **Berikutnya**.

![6.1-pasang](6.1-pasang.webp)

## 7. Zona Waktu

Pada tab **Lokasi**, Gantilah Wilayah **Asia** dan Zona **Jakarta**. Zona ini tergantung dari daerah Anda berada. Kemudian klik **Berikutnya**.

![7.1-pasang](7.1-pasang.webp)

## 8. Papan ketik

Pada tab **Papan Ketik**, Gantilah dari *Indonesia Arab (Melayu,phoenetic)* ke **English (US)** dengan jenis **Default**. Jenis papan ketik ini tergantung papan ketik yang Anda gunakan. Kemudian klik **Berikutnya**.

![8.1-pasang](8.1-pasang.webp)

## 9. Partisi Manual

Pada tab **Partisi**, Pilihlah **Pemartisian manual**

![9.1-pasang](9.1-pasang.webp)

Jika Anda pengguna GNU/Linux yang sebelumnya memiliki partisi **/home** tidak perlu membuat **Partisi Baru Tabel**, kecuali Anda ingin membangun sistem partisi baru lagi dari awal. Jika tidak punya **/home** maka buatlah juga **Partisi Baru tabel**.

Pada dokumentasi ini akan menggunakan skema partisi sebagai berikut.

### MBR

|||||
|:--|:--|:--|:--|
|**Partisi**|**Ukuran**|**Tipe**|**Flag**|
|/boot (MBR)|256 Mib|Fat32|boot|
|linuxswap|4096 Mib|swap|swap|
|/|51200 Mib|Ext4|root|
|/home|(sisa)|Ext4|-|

### UEFI

|||||
|:--|:--|:--|:--|
|**Partisi**|**Ukuran**|**Tipe**|**Flag**|
|/boot/efi|256 Mib|Fat32|boot|
|linuxswap|4096 Mib|swap|swap|
|/|51200 Mib|Ext4|root|
|/home|(sisa)|Ext4|-|

### Catatan

* 256 Mib = 0,25 GB
* 4096 Mib = 4 GB
* 51200 Mib = 50 GB

## 10. Langkah partisi di MBR (LEGACY)
Jika menggunakan partisi **MBR**, maka langkah-langkahnya sebagai berikut ini.

* Buatlah Tabel baru. Klik **Partisi Baru Tabel**.

![10.1-pasang](10.1-pasang.webp)

* Pilihlah **MBR**.

![10.2-pasang](10.2-pasang.webp)

* Pilih **Ruang kosong**, klik **membuat**, Ukuran: **256** Mib, Sistem berkas:**fat32**, Titik kait: **/boot**, Tanda: **boot**.

![10.3-pasang](10.3-pasang.webp)

* Pilih **Ruang kosong**, klik **membuat**, Ukuran: **4096** Mib, Sistem berkas:**linuxswap**, Titik kait: **-**, Tanda: **swap**.

![10.4-pasang](10.4-pasang.webp)

* Pilih **Ruang kosong**, klik **membuat**, Ukuran: **51200** Mib, Sistem berkas:**ext4**, Titik kait: **/**.

![10.5-pasang](10.5-pasang.webp)

* Pilih **Ruang kosong**, klik **membuat**, Ukuran: **(sisanya)** Mib, Sistem berkas:**ext4**, Titik kait: **/home**.

![10.6-pasang](10.6-pasang.webp)

* Kemudian klik **Berikutnya**.

![10.7-pasang](10.7-pasang.webp)

### 11. Langkah partisi di GPT (UEFI)

(Lewati panduan ke-11 ini jika Anda menggunakan MBR)

Jika menggunakan partisi **GPT**, maka langkah-langkahnya sebagai berikut.

* Buatlah Tabel baru. Klik **Partisi Baru Tabel**.

![11.1-pasang](11.1-pasang.webp)

* Pilihlah **GPT**.

![11.2-pasang](11.2-pasang.webp)

* Pilih **Ruang kosong**, klik **membuat**, Ukuran: **256** Mib, Sistem berkas:**fat32**, Titik kait: **/boot/efi**, Tanda: **boot**, **esp**.

![11.3-pasang](11.3-pasang.webp)

* Pilih **Ruang kosong**, klik **membuat**, Ukuran: **4096** Mib, Sistem berkas:**linuxswap**, Titik kait: **-**, Tanda: **swap**. (Jika Anda menggunakan SSD sebaiknya tidak perlu membuat partisi linuxswap)

![11.4-pasang](11.4-pasang.webp)

* Pilih **Ruang kosong**, klik **membuat**, Ukuran: **51200** Mib, Sistem berkas:**ext4**, Titik kait: **/**.

![11.5-pasang](11.5-pasang.webp)

* Pilih **Ruang kosong**, klik **membuat**, Ukuran: **(sisanya)** Mib, Sistem berkas:**ext4**, Titik kait: **/home**.

![11.6-pasang](11.6-pasang.webp)

* Kemudian klik **Berikutnya**.

![11.7-pasang](11.7-pasang.webp)

## 12. Data pengguna

Pada tab **Pengguna**, Isilah data Anda.

* Siapakah nama Anda: Isilah nama Lengkap anda. Misalnya "John Doe"
* Nama apa yang anda gunakan untuk login: Isilah nama tanpa spasi dan gunakan huruf kecil. Misalnya "johndoe"
* Apakah nama komputer ini: Isilah tanpa spasi dan gunakan huruf kecil. Misalnya "manjaro"
* Pilih sebuah kata sandi: isilah kata sandi Anda
* Ceklis **Gunakan sandi yang sama untuk akun administrator**.
* Kemudian klik **Berikutnya**.

![12.1-pasang](12.1-pasang.webp)

## 13. Proses pemasangan

Pada tab **Ikhtisar**, klik **install**.

![13.1-pasang](13.1-pasang.webp)

Kemudian muncul dialog baru dan klik **install sekarang**.

![13.2-pasang](13.2-pasang.webp)

Proses pemasangan Manjaro-X sedang berjalan.

![13.3-pasang](13.3-pasang.webp)

![13.4-pasang](13.4-pasang.webp)

Ketika pemasangan Manjaro-X sudah berhasil. Pada tab **Selesai**, ceklis **Mulai ulang sekarang** kemudian klik **Kelar**.

Selanjutnya komputer akan menyalakan ulang.

![13.5-pasang](13.5-pasang.webp)

Kemudian Cabut Flasdisknya ketika komputer dalam keadaan mati. **Selesai**.

## 14. Setelah memasang Manjaro-X

Setelah memasang Manjaro-X biasanya mengganti mirror seluruh dunia menjadi mirror lokal, agar lebih cepat ketika memperbarui sistem. Setelah mengganti mirror lokal, sistem membutuhkan pembaruan. Caranya sebagai berikut.

Selanjutnya mengatur mirror lokal. Buka Aplikasi **Tambah/Hapus Piranti Lunak**.

![14.1-paska-pasang](14.1-paska-pasang.webp)

Pilih Menu dan Buka **Preferensi**.

![14.2-paska-pasang](14.2-paska-pasang.webp)

Pilih Tab **Repositori Resmi**, Gunakan cermin dari **Indonesia**. Kemudian **Segarkan Daftar Cermin**.

![14.3-paska-pasang](14.3-paska-pasang.webp)

Pilih Menu, klik **Segarkan basis data**.

![14.4-paska-pasang](14.4-paska-pasang.webp)

Perbarui sistem operasi. Klik tab **Update**. Kemudian klik **terapkan** jika ada pembaruan.

![14.5-paska-pasang](14.5-paska-pasang.webp)

## 15. Pilihan Opsional.

* Anda bisa merubah lagi **Boot Order** di menu Bios dengan urutan HDD/SSD sebagai urutan pertama.
* **[Cara memformat Bootable di GNU/Linux](/documentation/format-bootable)**

## Perlu Bantuan?

Bergabunglah di komunitas telegram Manjaro Indonesia:

* **Komunitas Telegram: [Manjaro-X](https://t.me/manjaro_x)**
* **Kanal Telegram: [Manjaro-X News](https://t.me/manjaro_x)**
* **Surel: [os.manjaro.x@gmail.com](mailto:os.manjaro.x@gmail.com)**
