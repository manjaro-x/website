---
title: Catatan rilis dan KDE Spin untuk Plasma Fans
excerpt: >-
  Rilis baru Manjaro-X 19.0.0 dengan 5 Varian KDE Spin.
subtitle: >-
  Manjaro-X 19. Akhirnya rilis juga.
date: '2020-02-25'
thumb_img_path: posts/catatan-rilis-manjaro-x-19-0-0-kde/cover_thumb.webp
content_img_path: posts/catatan-rilis-manjaro-x-19-0-0-kde/cover.webp
layout: post
---

# KDE Spin Dengan 5 Variasi

Ide ini muncul ketika tahun 2016. Mirip Fedora Spin dan Labs yang menjadikan iso lebih bervariasi. Agar lebih memudahkan untuk orang awam maka dibuatlah KDE-Spin yang lebih variatif di Manjaro GNU/Linux. Yaitu: KDE Spin Developer, Design suite, Melody, CAD, dan Finance. Silahkan cek di [KDE Spin-Off](/spin_off)

# Tampilan Web Baru

Tampilan web baru lebih dioptimalkan dibagian visual dan aset, sehingga load lebih ringan dan tampilan lebih segar. Web ini diberdayakan oleh hugo dan netlify.

# Ukuran GNOME lebih kecil.

Mengurangi fonta yang tidak diperlukan seperti Noto-CJK. Sebelumnya 2.7GB kemudian menjadi 2.3 GB.

# Browser Firefox

Baik GNOME maupun KDE browser bawaan adalah Mozilla Firefox. Sehingga dapat ditambahkan Ekstensi oleh pengguna sesuai kebutuhan.

Agar pengguna lebih peduli dengan privasinya, maka pengembang menanamkan ekstensi di Manjaro-X, seperti:

+ firefox-adblock-plus
+ firefox-extension-https-everywhere
+ firefox-extension-privacybadger
+ firefox-ublock-origin

Nah, semoga apa yang dibuat oleh pengembang bermanfaat untuk kalangan pendidikan, pengembang, desainer, musisi dan berbagai bidang lainnya. Terlebih khususnya untuk orang awam.


Hervy Qurrotul ~ [@hervyqa](https://hervyqa.com)
