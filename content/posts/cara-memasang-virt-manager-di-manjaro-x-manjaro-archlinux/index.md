---
draft: false
title: "Cara Memasang Virt-Manager Di Manjaro-X, Manjaro, dan Archlinux"
excerpt: >-
  Virt-Manager merupakan GUI virtualisasi QEMU, sehingga menjalankan virtualisasi berkas ISO menjadi lebih mudah.
subtitle: >-
  Panduan berikut merupakan cara praktis memasang virt-manager di distribusi archlinux dan turunanannya.
date: 2020-03-04T13:38:51+07:00
thumb_img_path: posts/cara-memasang-virt-manager-di-manjaro-x-manjaro-archlinux/cover_thumb.webp
content_img_path: posts/cara-memasang-virt-manager-di-manjaro-x-manjaro-archlinux/cover.webp
layout: post
---

# Virt-Manager

Virt-Manager merupakan GUI virtualisasi QEMU, sehingga menjalankan virtualisasi berkas ISO menjadi lebih mudah. Tak hanya itu Virt-Manager juga mampu menjalankan Hypervisor.

# Memasang Virt-Manager

Langsung saja bukalah konsole atau terminal emulator lainnya. kemudian jalankan perintah ini.

```
sudo pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat ebtables iptables
```

Depesensi tersebut sudah tersedia di [Manjaro-X] edisi Developer versi 19.0 keatas.

Kemudian jalankan layanan libvirtd.

```
sudo systemctl start libvirtd
```

# Memasang Virtual ISO

Pilih menu Virtual Machine Manager

![Buka Virt Manager](1-virt-manager.webp)

Kemudian masukkan kata sandi pengguna.

![Kata sandi virtmanager](2-masukkan-kata-sandi-virt-manager.webp)

Pilih **File > Add Connection...**.

Pilih **QEMU/KVM**, kemudian klik **Connect**.

![Pilih Qemu](3-pilih-qemu.webp)

Selanjutnya pilih **local install media**.

![Pilih Local ISO](4-pilih-iso.webp)

Pilih **Browse** untuk memilih direktori ISO.

![Browse](5-browse.webp)

Kemudian buat baru tempat penyimpanan virtualnya. klik tanda **+** di bawah.

![Storage Volume](6-pilih-penyimpanan.webp)

Gantilah nama **pool** menjadi nama virtualisasinya. misalnya **manjaro-x**.

![Ganti Nama](7-ganti-nama.webp)

Klik **Browse**, kemudian pilih lokasi direktori ISO.

![Pilih Direktori ISO](8-pilih-direktori-iso.webp)

Hingga tampak seperti ini.

![Setting Penyimpanan](9-mengatur-menyimpanan.webp)

Setelah mengatur pnyimpanan volume. Pilihlah ISO yang akan digunakan. Kemudian Klik **Choose Volume**.

![Memilih ISO](10-memilih-iso.webp)

Hapus tanda ceklis **Automatic detect from the installation media / source**

Kemudian pilih jenis distribusi. Misalnya Arch Linux, Ubuntu 19.10, Microsoft Windows 10, atau distribusi ISO lainnya.

![Memilih distro](11-jenis-distro.webp)

Lalu pilih nilai Memori dan CPU yang akan digunakan. Misalnya 2048 untuk memori dan 2 untuk CPU.

![Nilai Virtual](12-nilai-virtual.webp)

Pilih alokasi penyimpanan diska. Misalnya **20,0 GB**

![Alokasi penyimpanan](13-alokasi-penyimpanan.webp)

Tunggu proses pembuatan volume.

![proses-volume](14-proses-volume.webp)

Kemudian muncul proses booting distribusi.

![Proses boot](15-proses-boot.webp)

Tunggu hingga tampilan distribusi tampil. Selesai.

![Tampilan distribusi](16-tampilan-distro.webp)

Mudah bukan. Silakan dicoba...

***
[Manjaro-X]:https://manjaro-x.netlify.com
