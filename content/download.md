---
title: Download
sections:
  - section_id: download
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/gnome.webp
    title: GNOME
    content: >-
      GNOME merupakan lingkungan desktop yang kuat dan mencoba menyederhanakan tampilan. Kesederhanaan dan kemudahan dalam pengoperasiannya untuk setiap orang. Jika RAM Anda lebih dari 4GiB maka cocok sekali Anda memakainya. Dalam edisi GNOME sudah tersedia perangkat lunak untuk perkantoran dan aktifitas sehari-hari. Seperti menjelajah web, menonton film, memutar audio, membuat catatan, dan lain sebagainya.
    actions:
      - label: GNOME
        url: /gnome
      - label: Lihat Demo
        url: 'https://www.youtube.com/watch?v=ae2D4aWTsXM'
  - section_id: download
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/kde.webp
    title: KDE Plasma
    content: >-
        KDE Plasma merupakan lingkungan desktop yang _simple by default_, sederhana dan cocok untuk pengguna yang baru saja bermigrasi ke GNU/Linux. KDE Plasma ini juga memungkinkan pengguna untuk mengerjakan suatu hal secara efisien. Tampilan KDE Plasma lebih dinamis, mulai dari desktop sederhana hingga desktop intuitif yang kaya fitur. Tentu KDE Plasma lebih ramah dan mudah dipakai, serta mempunyai cara kerja yang elegan dan efisien. Sama seperti edisi GNOME, KDE Plasma sudah tersedia perangkat lunak untuk perkantoran dan aktifitas sehari-hari.
    actions:
      - label: KDE Plasma
        url: /kde
      - label: Lihat Demo
        url: 'https://www.youtube.com/watch?v=Dd-HCe-hHCc'
  - section_id: call-to-action
    component: cta_block.html
    type: ctablock
    title: Lebih Produktif? Cobalah KDE Spin-Off!
    subtitle: Pilih yang Anda perlukan dan jadilah diri sendiri!.
    actions:
      - label: Lebih Lanjut
        url: '/spin_off'
layout: download
menu:
  main:
    weight: 3
    name: Unduh
---
