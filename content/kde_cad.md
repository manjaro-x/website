---
title: Unduh KDE CAD
sections:
  - section_id: single
    component: content_block.html
    type: contentblock
    bg: gray
    image: images/kde_cad.webp
    title: Unduh KDE CAD
    content: >-
      Silahkan klik Unduh ISO untuk mulai mengunduh.
      
      ## Sesi Masuk
      
        |||
        |:--:|:--:|:--:|
        |**Nama Pengguna**|**Kata Sandi**|
        |user|user|
      
      ## Checksum
      
        ||||
        |:--:|:--:|:--:|:--:|
        |**MD**|**SHA1**|**SHA256**|
        | bf1814dd4a1 c08e6a0effb ca0142a7bc | 8c4a57c664a af8f75aa31a 7567ef0800 89d95725 | f06f97e81f5 5497591eca7 3f8362d55b9 cfac85b5354 4cc53f7ddb1 02c569b1e |

    actions:
      - label: Unduh ISO
        url: 'https://manjaro-x.id/release/20.1/manjaro-x-kde-cad-20.1-linux54-x86_64.iso'
      - label: Lihat Paket
        url: 'https://manjaro-x.id/release/20.1/manjaro-x-kde-cad-20.1-linux54-x86_64-pkgs.txt'
  - section_id: call-to-action
    component: cta_block.html
    type: ctablock
    title: Charity OS!
    subtitle: "Manjaro-X bisa didapatkan/diunduh secara gratis, namun membuatnya tidaklah gratis dan mudah. Kami sangat berterima kasih kepada Anda yang telah menggunakan Manjaro-X sebagai sistem operasi utama Anda. Jika dirasa Manjaro-X sangat bermanfaat untuk Anda secara pribadi maupun untuk Perusahaan /Lembaga /Universitas, Anda dapat mendonasikan kepada para pengembang untuk pembangunan dan pengujian Manjaro-X agar perkembangannya bisa berkelanjutan."
    actions:
      - label: Yuk, Donasi!
        url: /donation
---
