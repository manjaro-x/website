---
title: Syarat dan Ketentuan
subtitle: >-
  Kebijakan Privasi ini mengatur cara Manjaro-X.
img_path: images/cover-manjaro-x.webp
layout: page
menu:
  secondary:
    weight: 3
    name: Syarat dan Ketentuan
---

## Penolakan

Tidak ada jaminan, baik tersurat maupun tersirat, dengan ini diberikan untuk apa pun yang disediakan oleh Manjaro-X GNU/Linux dan Manjaro Linux ("Perangkat Lunak"). Semua perangkat lunak disediakan tanpa jaminan yang menyertainya, baik yang disebutkan secara tersurat, tersirat atau diasumsikan secara diam-diam. Informasi ini tidak termasuk jaminan mengenai kualitas, tidak menggambarkan kualitas yang dapat dipasarkan yang adil, dan tidak membuat klaim mengenai jaminan kualitas atau jaminan mengenai kesesuaian untuk tujuan khusus. Pengguna menanggung semua tanggung jawab atas kerusakan yang diakibatkan oleh penggunaan perangkat lunak.

## Perangkat Lunak Bebas, Manjaro-X dan Manjaro Linux

Manjaro-X merupakan pemasang Manjaro Linux yang lengkap untuk orang awam, desainer, pengajar, animator, dan pengembang aplikasi GTK. Setelah memasang Manjaro-X versi rilis sistem operasi akan kembali menggunakan Manjaro linux. Sebab Manjaro-X bukanlah distribusi dengan branding setelah dipasang, melainkan sistem operasi Manjaro Linux yang di bundel kembali untuk kepraktisan penggunaan.

Tujuan Manjaro-X ini hanyalah untuk kepentingan pendidikan GNU/Linux semata, agar tercapainya kemudahan dan kenyamanan bagi pengguna yang pertama kali melihat sistem operasi GNU/Linux. Tujuan lainnya Manjaro-X ini untuk memperkaya jenis distribusi Manjaro Linux tek resmi sebagai sistem operasi GNU/Linux yang sederhana, kuat, dan elegan.

Manjaro mengembangkan perangkat lunak gratis. Semua alat Manjaro Linux (Pamac, MSM, Manjaro-Tools) adalah dan akan selalu menjadi perangkat lunak gratis. Manjaro Linux adalah bagian dari [OIN](http://www.openinventionnetwork.com/community-of-licensees/) sejak Maret 2014. Lisensi saat ini dapat dilihat [di sini](http://www.openinventionnetwork.com/joining-oin/oin-license-agreement/). Informasi tambahan tentang paket-paket yang dicakup oleh lisensi ini dapat dilihat [di sini](http://www.openinventionnetwork.com/joining-oin/linux-system/).

Bukan tujuan kami untuk menjadikan Manjaro distro yang bebas dari perangkat lunak berpemilik, seperti [Trisquel](http://trisquel.info/) atau [gNewSense](http://www.gnewsense.org/). Meskipun Anda dapat menggunakan Manjaro tanpa perangkat lunak tidak bebas, repositori kami juga berisi perangkat lunak tidak bebas yang dapat Anda instal. Jadi, walaupun akan selalu dimungkinkan untuk memiliki Manjaro dengan perangkat lunak gratis saja, kami akan mengirimkan perangkat lunak apa pun yang kami bisa selama pengguna kami memintanya dan pengemas kami bersedia untuk menyiapkannya untuk mereka.

Jika Anda ingin memeriksa lisensi suatu paket, Anda dapat melakukannya dengan Pacman. Juga, periksa apakah ada folder bernama /usr/share/licenses/ di sistem Anda; jika demikian, itu harus berisi informasi lisensi untuk paket itu.

Media langsung kami memberi Anda pilihan untuk boot dengan driver gratis atau tidak, sehingga Anda dapat memeriksa sebelum menginstal mana yang ingin Anda gunakan.

## Ucapan Terima Kasih

Kami ingin mengucapkan terima kasih kepada semua orang yang telah memungkinkan kami untuk membuat Manjaro-X dan / atau berkontribusi padanya. Ini secara khusus:

* Pengembang dan komunitas Manjaro Linux, untuk menciptakan distribusi lengkap dengan MHWD.
* Pengembang dan komunitas Arch Linux, untuk menciptakan distribusi ™ sempurna.
* Pengembang Pacman, untuk menciptakan pengelola paket terbaik yang terbaik di dunia.
* Proyek Gnome, untuk lingkungan desktop ringan yang kami gunakan sebagai default.
* Proyek KDE, untuk menciptakan lingkungan desktop paling kuat di planet bumi.
* Proyek XFCE, untuk lingkungan desktop hebat lainnya.
* Semua orang menyediakan unduhan dan paket mirror. Tanpa Anda, kami tidak akan berada di sini.
* Semua penguji yang mengambil misi untuk menguji barang-barang kami dengan sabar.
* Semua orang benar-benar membaca dokumentasi.

## Merek Dagang.

Manjaro-X GNU/Linux menggunakan beberapa merek dagang dari berbagai proyek. Hak-hak mereka tidak ditolak oleh lisensi kami dan tetap utuh.

* Manjaro, hak cipta © 2019 Manjaro GmbH & Co. KG
* Nama dan logo Manjaro adalah merek dagang yang diakui dari Manjaro GmbH & Co. KG. Beberapa hak dilindungi. Komunitas Manjaro dilisensikan untuk menggunakan merek dagang dengan ketentuan yang diberikan.
* Manjaro Linux, hak cipta © 2011-2018 Philip Müller dan Pengembang Manjaro
* Arch Linux, hak cipta © 2002-2019 Judd Vinet dan Aaron Griffin
* GNOME dilisensikan berdasarkan Perjanjian Lisensi Yayasan GNOME
* KDE® dan logo K Desktop Environment® adalah merek dagang terdaftar dari KDE e.V.
* Merek dagang terdaftar Linux® digunakan sesuai dengan sublisensi dari LMI, pemegang lisensi eksklusif Linus Torvalds, pemilik merek pada basis di seluruh dunia.
* Semua merek dagang dan hak cipta lainnya adalah milik dari pemiliknya masing-masing dan hanya disebutkan untuk tujuan informatif.

## Kehadiran Web Manjaro-X

* Konsep: [Hervy Qurrotul Ainur](mailto:hervyqa@gmail.com)
* Desain & Tukang Kompil: [Hervy Qurrotul Ainur](mailto:hervyqa@gmail.com)
* Pemberdaya Web: [M. Nabil Adani](mailto:nblid48@gmail.com)
* Surel: [os.manjaro.x@gmail.com](mailto:os.manjaro.x@gmail.com)

## Informasikan ke resolusi Perselisihan

Platform ini akan menjadi titik masuk untuk penyelesaian sengketa di luar pengadilan yang timbul dari penjualan online dan kontrak layanan yang dibuat antara konsumen dan pedagang. Manjaro-X tidak berkewajiban atau dipersiapkan untuk menghadiri prosedur penyelesaian sengketa sebelum entitas penyelesaian sengketa alternatif.

## Dibawah tenda

Situs ini diberdayakan oleh:

* [Hugo](https://gohugo.io)
* [Gitlab](https://gitlab.com)
* [Netlify](https://netlify.com)
