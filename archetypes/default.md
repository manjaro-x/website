---
draft: true
title: "{{ replace .Name "-" " " | title }}"
excerpt: >-
  Description for front blog
subtitle: >-
  Description for SEO content
date: {{ .Date }}
thumb_img_path: posts/{{ replace .Name " " "-" }}/cover_thumb.webp
content_img_path: posts/{{ replace .Name " " "-" }}/cover.webp
layout: post
---

# Title Blog

Lorem Ipsum Paragraph [Manjaro-X].

## Sub-title Blog

Lorem Ipsum Paragraph [Manjaro-X].

***
[Manjaro-X]:https://manjaro-x.netlify.com
